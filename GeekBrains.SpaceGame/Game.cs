﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using GeekBrains.CSharpSecond.SpaceGame.ObjectParams;

namespace GeekBrains.CSharpSecond.SpaceGame
{
	/// <summary>
	/// Класс игры для отрисовки и обработки
	/// </summary>
	public static class Game
	{

		private static BufferedGraphicsContext _context;

		/// <summary>
		/// Поле буфера кадра
		/// </summary>
		public static BufferedGraphics Buffer;


		/// <summary>
		/// Свойство Ширины игрового поля
		/// </summary>
		public static int Width { get; set; }

		/// <summary>
		/// Свойство Высоты игрового поля
		/// </summary>
		public static int Height { get; set; }

		/// <summary>
		/// Конструктор по умолчанию
		/// </summary>
		static Game()
		{

		}

		/// <summary>
		/// Метод инициализации элементов игры
		/// </summary>
		/// <param name="form">Форма в которую будет идти отрисовка игры</param>
		public static void Init(Form form)
		{
			// Графическое устройство для вывода графики
			Graphics g;
			// Предоставляет доступ к главному буферу графического контекста для текущего приложения
			_context = BufferedGraphicsManager.Current;
			g = form.CreateGraphics();

			// Создаем объект (поверхность рисования) и связываем его с формой
			// Запоминаем размеры формы
			Width = form.Width;
			Height = form.Height;

			// Связываем буфер в памяти с графическим объектом, чтобы рисовать в буфере
			Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));

			// Подгружаем объекты
			Load();

			// Создаём таймер
			var timer = new Timer {Interval = 100};
			timer.Start();
			timer.Tick += Timer_Tick;
		}

		private static void Timer_Tick(object sender, EventArgs e)
		{
			Draw();
			Update();
		}

		/// <summary>
		/// Метод отрисовки игры
		/// </summary>
		public static void Draw()
		{
			Buffer.Graphics.Clear(Color.Black);
			foreach (BaseObject obj in _objs)
				obj.Draw();
			Buffer.Render();
		}

		/// <summary>
		/// Обновление всех входящих элементов
		/// </summary>
		public static void Update()
		{
			foreach (BaseObject obj in _objs)
				obj.Update();
		}

		/// <summary>
		/// Массив хранения объектов
		/// </summary>
		public static BaseObject[] _objs;

		/// <summary>
		/// Метод загрузки объектов для инициализации
		/// </summary>
		public static void Load()
		{
			int i = 0;
			_objs = new BaseObject[30];
			_objs[i++] = new Nebula(
				new BaseObjectParams
				{
					Position = new Point(300, 300),
					Direction = new Point(-5, 0),
					ObjectSize = new Size(100, 50)
				});
			_objs[i++] = new BigStar(
				new ImageObjectParams
				{
					Position = new Point(100, 100),
					ObjectSize = new Size(100, 100)
				});

			for (; i < _objs.Length; i++)
				_objs[i] = new Star(
					new BaseObjectParams
					{
						Position = new Point(10, i * 20),
						Direction = new Point(-i, 0),
						ObjectSize = new Size(3, 3)
					});
		}
	}
}
