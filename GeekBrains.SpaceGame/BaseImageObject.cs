﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using GeekBrains.CSharpSecond.SpaceGame.ObjectParams;

namespace GeekBrains.CSharpSecond.SpaceGame
{
	/// <summary>
	/// Класс базового объекта
	/// </summary>
	public class BaseImageObject : BaseObject
	{
		/// <summary>
		/// Расположение
		/// </summary>
		protected Image Img;

		/// <summary>
		/// Конструктор базового объекта с изображением
		/// </summary>
		/// <param name="param">Параметры объекта</param>
		public BaseImageObject(ImageObjectParams param) : base(param)
		{
			Img = param.ObjectImage;
		}

		/// <summary>
		/// Отрисовка базового объекта
		/// </summary>
		public override void Draw()
		{
			Rectangle rect = new Rectangle(Pos.X - Size.Width / 2, Pos.Y - Size.Height / 2, Size.Width, Size.Height);
			Game.Buffer.Graphics.DrawImage(Img, rect);

		}

		/// <summary>
		/// Обновление состояния базового объекта
		/// </summary>
		public override void Update()
		{
			Pos.X = Pos.X + Dir.X;
			Pos.Y = Pos.Y + Dir.Y;
			if (Pos.X < 0)
				Dir.X = -Dir.X;
			if (Pos.X > Game.Width)
				Dir.X = -Dir.X;
			if (Pos.Y < 0)
				Dir.Y = -Dir.Y;
			if (Pos.Y > Game.Height)
				Dir.Y = -Dir.Y;
		}
	}
}
