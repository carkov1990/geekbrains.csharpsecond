﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;


namespace GeekBrains.CSharpSecond.SpaceGame
{
	/// <summary>
	/// Главный класс
	/// </summary>
	static class Program
	{
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		//[STAThread]
		static void Main()
		{
			Form mainForm = new Form
			{
				Width = 800,
				Height = 600
			};

			Game.Init(mainForm);
			mainForm.Show();
			Game.Draw();

			Application.Run(mainForm);
		}
	}
}
