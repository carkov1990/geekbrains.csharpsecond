﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekBrains.CSharpSecond.SpaceGame.Properties;

namespace GeekBrains.CSharpSecond.SpaceGame.ObjectParams
{
	/// <summary>
	/// Класс параметров объекта космического пространства
	/// </summary>
	public class ImageObjectParams : BaseObjectParams
	{
		/// <summary>
		/// Изображение
		/// </summary>
		public Image ObjectImage { get; set; } = Resources.bigStar2;
	}
}
