﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekBrains.CSharpSecond.SpaceGame.ObjectParams
{
	/// <summary>
	/// Базовый класс настроек объекта
	/// </summary>
	public class BaseObjectParams
	{
		/// <summary>
		/// Позиция объекта
		/// </summary>
		public Point Position { get; set; }

		/// <summary>
		/// Дельта направления движения
		/// </summary>
		public Point Direction { get; set; }

		/// <summary>
		/// Размер
		/// </summary>
		public Size ObjectSize { get; set; }
	}
}
