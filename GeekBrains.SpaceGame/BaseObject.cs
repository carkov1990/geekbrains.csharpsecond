﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using GeekBrains.CSharpSecond.SpaceGame.ObjectParams;

namespace GeekBrains.CSharpSecond.SpaceGame
{
	/// <summary>
	/// Класс базового объекта
	/// </summary>
	public class BaseObject
	{
		/// <summary>
		/// Расположение
		/// </summary>
		protected Point Pos;

		/// <summary>
		/// Направление
		/// </summary>
		protected Point Dir;

		/// <summary>
		/// Размер
		/// </summary>
		protected Size Size;

		/// <summary>
		/// Конструктор базового объекта
		/// </summary>
		/// <param name="param">Параметры объекта</param>
		public BaseObject(BaseObjectParams param)
		{
			Pos = param.Position;
			Dir = param.Direction;
			Size = param.ObjectSize;
		}

		/// <summary>
		/// Отрисовка базового объекта
		/// </summary>
		public virtual void Draw()
		{
			Game.Buffer.Graphics.DrawEllipse(Pens.White, Pos.X, Pos.Y, Size.Width, Size.Height);
		}

		/// <summary>
		/// Обновление состояния базового объекта
		/// </summary>
		public virtual void Update()
		{
			Pos.X = Pos.X + Dir.X;
			Pos.Y = Pos.Y + Dir.Y;
			if (Pos.X < 0)
				Dir.X = -Dir.X;
			if (Pos.X > Game.Width)
				Dir.X = -Dir.X;
			if (Pos.Y < 0)
				Dir.Y = -Dir.Y;
			if (Pos.Y > Game.Height)
				Dir.Y = -Dir.Y;
		}
	}
}
