﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using GeekBrains.CSharpSecond.SpaceGame.ObjectParams;

namespace GeekBrains.CSharpSecond.SpaceGame
{
	/// <summary>
	/// Класс Звёзд
	/// </summary>
	public class BigStar : BaseImageObject
	{
		private readonly int _speedMax = 5;
		private static readonly int _minSize = 50;
		private static readonly int _maxSize = 250;

		private static int GetLimitValue(int size)
		{
			return size < 50 ? 50 : 250;
		}

		private static Size GetSize(Size size)
		{
			var height = size.Height;
			var width = size.Width;

			size.Height = height >= _minSize && height <= _maxSize ? height : GetLimitValue(height);
			size.Width = width >= _minSize && width <= _maxSize ? width : GetLimitValue(width);
			return size;
		}

		/// <summary>
		/// Конструктор класса звезды
		/// </summary>
		/// <param name="param">Параметры объекта</param>
		public BigStar(ImageObjectParams param) : base(param)
		{
			Dir = new Point(-(_speedMax - (GetSize(param.ObjectSize).Width / 50)), 0);
			Size = GetSize(param.ObjectSize);
		}

		/// <summary>
		/// Метод рисования
		/// </summary>
		public override void Draw()
		{
			// Pos.X Pos.Y - центр звезды
			Rectangle rect = new Rectangle(Pos.X - Size.Width / 2, Pos.Y - Size.Height / 2, Size.Width, Size.Height);
			Game.Buffer.Graphics.DrawImage(Img, rect);
		}

		/// <summary>
		/// Метод обновления
		/// </summary>
		public override void Update()
		{
			Pos.X += Dir.X;
			if (Pos.X < -Size.Width)
			{
				Random rnd = new Random(Pos.Y);
				Pos.X = Game.Width + Size.Width;
				Pos.Y = (rnd.Next() % (Game.Height - 120)) + 60;
				int newsize = ((rnd.Next() % 3) + 1) * 50;
				Size = new Size(newsize, newsize);
				Dir.X = -2 * (_speedMax - Size.Width / 50);
			}
		}

	}
}

